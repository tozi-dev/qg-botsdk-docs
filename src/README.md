---
home: true
icon: home
title: Home
heroImage: /logo.svg
bgImage: https://theme-hope-assets.vuejs.press/bg/6-light.svg
bgImageDark: https://theme-hope-assets.vuejs.press/bg/6-dark.svg
bgImageStyle:
  background-attachment: fixed
heroText: QG-botsdk
tagline: Fast and efficient implementation of the QQ channel robot SDK.
actions:
  - text: Start 💡
    link: ./zh/guide/
    type: primary

  - text: Docs
    link: ./zh/docs/

highlights:
  - header: Multiple application layer development methods
    image: /assets/image/truck-fast-solid.svg
    bgImage: https://theme-hope-assets.vuejs.press/bg/3-light.svg
    bgImageDark: https://theme-hope-assets.vuejs.press/bg/3-dark.svg
    highlights:
      - title: Support threading and asyncio.
      - title: Both use high performance underlying implementations.
---

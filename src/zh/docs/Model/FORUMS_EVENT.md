# FORUMS_EVENT
-   官方目前仍在开发此事件，目前的字段存在许多冗余，因此后续很有可能随时出现变化。

-   如遇到官方更改字段的相关情况，请马上 [联系作者](/联系与反馈) 进行修改，后下载最新release使用>

-   `thread_info`字段较为复杂，具体可视化结构如下：

```text
thread_info
├── title
|   ├── paragraphs (list)
|   |   ├── elems (list)
|   |   |   ├── type
|   |   |   ├── text
|   |   |   |   ├── text
|   |   ├── props
├── content
|   ├── paragraphs (list)
|   |   ├── elems (list)
|   |   |   ├── type
|   |   |   ├── 根据type字段判断此字段，目前可能的值：text、url、(image、video)
|   |   ├── props
```
## 结构
| 结构          |        |                                 |

| 字段名         | 类型     | 说明                              |
| ----------- | ------ | ------------------------------- |
| thread_info | object | 帖子数据详情，其子类请参阅下表【结构：thread_info】 |
| author_id   | string | 帖子作者的频道ID                       |
| date_time   | string | 帖子发布事件（ISO8601 timestamp）       |
| channel_id  | string | 子频道ID                           |
| guild_id    | string | 频道ID                            |
| thread_id   | string | 帖子ID                            |
| t           | string | 事件类型字段，如GUILD_CREATE            |
| event_id    | sring  | 事件ID                            |

| 结构：thread_info |        |                         |
| -------------- | ------ | ----------------------- |
| 字段名            | 类型     | 说明                      |
| title          | object | 标题数据，其子类请参阅【结构：sub_thr】 |
| content        | object | 标题数据，其子类请参阅【结构：sub_thr】 |

| 结构：sub_thr |      |                       |
| ---------- | ---- | --------------------- |
| 字段名        | 类型   | 说明                    |
| paragraphs | list | 段落列表，其每一项请参阅【结构：para】 |

| 结构：para |        |                                                        |
| ------- | ------ | ------------------------------------------------------ |
| 字段名     | 类型     | 说明                                                     |
| elems   | list   | 内容元素，其每一项请分别参阅——title：【结构：elems_t】content：【结构：elems_c】 |
| props   | object | 作用未名，空object数据                                         |

| 结构：elems_t |        |                       |
| ---------- | ------ | --------------------- |
| 字段名        | 类型     | 说明                    |
| text       | object | 包含一个子类text，其类型为string |
| type       | int    | 元素类型，可能的值：1（普通文本）     |

| 结构：elems_c |        |                                      |
| ---------- | ------ | ------------------------------------ |
| 字段名        | 类型     | 说明                                   |
| text       | object | 包含一个子类text，其类型为string                |
| url        | object | 包含两个子类：url desc，其类型均为string          |
| type       | int    | 元素类型，可能的值：1（普通文本）、2（图片）、3（视频）、4（url） |

> 现有推送和相关信息的type字段：

> type 1：普通文本，子字段text

> type 2：图片，子字段image（曾短暂出现，目前为空子字段，无任何内容反馈）

> type 3：视频，子字段video（曾短暂出现，目前为空子字段，无任何内容反馈）

>

> type 4：url信息，子字段url

>

>

>

> 现无推送，根据文档列出的type：

> 原type 2：at信息，目前为空子字段，无任何内容反馈

> 原type 4：表情，目前为空子字段，无任何内容反馈

> 原type 5：##子频道，目前为空子字段，无任何内容反馈

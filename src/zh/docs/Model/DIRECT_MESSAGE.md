# DIRECT_MESSAGE
## 结构

| 字段名               | 类型     | 说明                                                      |
| ----------------- | ------ | ------------------------------------------------------- |
| author            | object | 发送消息成员的详细信息，其子类请参阅下表【结构：author】                         |
| channel_id        | sring  | 子频道ID                                                   |
| guild_id          | sting  | 频道ID                                                    |
| content           | string | 消息内容                                                    |
| message_reference | object | 该条消息的引用消息数据，如没有引用则没有此字段，其子类请参阅下表【结构：message_reference】  |
| attachments       | object | 该条消息的附件（图片）数据，如没有附件则没有此字段，列表中的子类请参阅下表【结构：Attachments】   |
| id                | string | 消息ID（用于发送被动消息）                                          |
| seq               | int    | 用于消息间的排序，seq 在同一子频道中按从先到后的顺序递增，不同的子频道之间消息无法排序           |
| seq_in_channel    | string | 子频道消息 seq，用于消息间的排序，seq 在同一子频道中按从先到后的顺序递增，不同的子频道之间消息无法排序 |
| src_guild_id      | string | 用于私信场景下识别真实的来源频道id                                      |
| timestamp         | string | 消息创建时间（ISO8601 timestamp）                               |
| t                 | string | 事件类型字段，如GUILD_CREATE                                    |
| event_id          | sring  | 事件ID                                                    |
| treated_msg       | string | 经过处理（包括去除艾特机器人、/等字段）的内容（绑定事件时treated_data=False则没有此字段）  |




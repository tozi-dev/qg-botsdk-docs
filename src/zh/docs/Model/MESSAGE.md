# MESSAGE
## 结构
| 字段名               | 类型     | 说明                                                      |
| ----------------- | ------ | ------------------------------------------------------- |
| author            | object | 发送消息成员的详细信息，其子类请参阅下表【结构：user】                         |
| channel_id        | sring  | 子频道ID                                                   |
| guild_id          | sting  | 频道ID                                                    |
| content           | string | 消息内容                                                    |
| message_reference | object | 该条消息的引用消息数据，如没有引用则没有此字段，其子类请参阅下表【结构：message_reference】  |
| mentions          | object | 该条消息@其他成员的数据，如没有艾特则没有此字段，列表中的子类请参阅下表【结构：Mentions】       |
| attachments       | object | 该条消息的附件（图片）数据，如没有附件则没有此字段，列表中的子类请参阅下表【结构：Attachments】   |
| id                | string | 消息ID（用于发送被动消息）                                          |
| seq               | int    | 用于消息间的排序，seq 在同一子频道中按从先到后的顺序递增，不同的子频道之间消息无法排序           |
| seq_in_channel    | string | 子频道消息 seq，用于消息间的排序，seq 在同一子频道中按从先到后的顺序递增，不同的子频道之间消息无法排序 |
| timestamp         | string | 消息创建时间（ISO8601 timestamp）                               |
| tts               | bool   | 未知详情，疑似text-to-speech语音合成（该字段不稳定出现，请勿用于运营应用环境）          |
| pinned            | bool   | 该消息是否精华消息（该字段不稳定出现，请勿用于运营应用环境）                          |
| type              | sring  | 未知详情，疑似消息类型（该字段不稳定出现，请勿用于运营应用环境）                        |
| flags             | sring  | 未知详情（该字段不稳定出现，请勿用于运营应用环境）                               |
| t                 | string | 事件类型字段，如GUILD_CREATE                                    |
| event_id          | sring  | 事件ID                                                    |
| treated_msg       | string | 经过处理（包括去除艾特机器人、/等字段）的内容（绑定事件时treated_data=False则没有此字段）  |




### Mentions
| 字段名         | 类型     | 说明           |
| ----------- | ------ | ------------ |
| avatar      | sring  | 被@成员头像图片url  |
| bot         | bool   | 被@成员是否机器人    |
| id          | string | 被@成员的ID      |
| username    | list   | 被@成员在频道全局的昵称 |



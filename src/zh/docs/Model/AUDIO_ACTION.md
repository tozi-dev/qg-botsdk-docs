# AUDIO_ACTION

-   只有AUDIO_START、AUDIO_FINISH拥有audio_url和text字段



| 字段名        | 类型     | 说明                   |
| ---------- | ------ | -------------------- |
| audio_url  | string | 音频url                |
| text       | string | 音频描述                 |
| channel_id | string | 子频道ID                |
| guild_id   | string | 频道ID                 |
| t          | string | 事件类型字段，如GUILD_CREATE |
| event_id   | sring  | 事件ID                 |
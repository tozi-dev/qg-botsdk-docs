# GUILD_MEMBERS
## 结构

| 字段名        | 类型     | 说明                                         |
| ---------- | ------ | ------------------------------------------ |
| guild_id   | sring  | 频道ID                                       |
| joined_at  | sring  | 该成员加入频道时间，具体格式例如：2021-10-21T11:20:18+08:00 |
| nick       | string | 成员在当前频道的昵称                                 |
| roles      | list   | 该成员拥有的身份组ID列表，当中每个身份组ID的类型为string          |
| user       | object | 该成员详细信息，其子类请参阅下表【结构：user】                  |
| op_user_id | string | 操作人ID                                      |
| t          | string | 事件类型字段，如GUILD_CREATE                       |
| event_id   | sring  | 事件ID                                       |


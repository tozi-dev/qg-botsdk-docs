---
title: CHANNELS
---
# CHANNELS
::: warning
需要SDK版本$\ge2.2.4$
:::

## 结构
| 字段名              | 类型     | 说明                                        |
| ---------------- | ------ | ----------------------------------------- |
| application_id   | sring  | 标识应用子频道应用类型                               |
| guild_id         | sring  | 频道ID                                      |
| id               | sring  | 子频道ID（channel id）                         |
| name             | sring  | 子频道名                                      |
| op_user_id       | string | 操作人ID                                     |
| owner_id         | string | 频道拥有者ID                                   |
| parent_id        | string | 所属分组 id，仅对子频道有效，对 子频道分组（ChannelType=4） 无效 |
| permissions      | string | 用户拥有的子频道权限                                |
| position         | int    | 排序值                                       |
| private_type     | int    | 子频道私密类型                                   |
| speak_permission | int    | 子频道发言权限                                   |
| sub_type         | int    | 子频道子类型                                    |
| type             | int    | 子频道类型                                     |
| t                | string | 事件类型字段，如GUILD_CREATE                      |
| event_id         | sring  | 事件ID                                      |
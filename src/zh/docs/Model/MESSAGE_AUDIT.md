# MESSAGE_AUDIT

## 结构
| 结构          |        |                                   |
| ----------- | ------ | --------------------------------- |
| 字段名         | 类型     | 说明                                |
| audit_id    | string | 消息审核ID                            |
| audit_time  | string | 消息审核时间（ISO8601 timestamp）         |
| create_time | string | 消息创建时间（ISO8601 timestamp）         |
| channel_id  | string | 子频道ID                             |
| guild_id    | string | 频道ID                              |
| message_id  | string | 成功后的消息ID（只有审核通过事件才会有message_id的值） |
| t           | string | 事件类型字段，如GUILD_CREATE              |
| event_id    | sring  | 事件ID                              |
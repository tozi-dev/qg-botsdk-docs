# MESSAGE_DELETE
## 结构


| 结构       |        |                                 |
| -------- | ------ | ------------------------------- |
| 字段名      | 类型     | 说明                              |
| message  | object | 被撤回消息的消息数据，其子类请参阅下表【结构：message】 |
| op_user  | object | 操作人数据，其子类只有id（类型为string）        |
| t        | string | 事件类型字段，如GUILD_CREATE            |
| event_id | sring  | 事件ID                            |

| 结构：message |        |                                |
| ---------- | ------ | ------------------------------ |
| 字段名        | 类型     | 说明                             |
| author     | object | 被撤回消息的作者数据，其子类请参阅下表【结构：author】 |
| id         | string | 被撤回消息的消息ID                     |
| channel_id | string | 子频道ID                          |
| guild_id   | string | 频道ID                           |

| 结构：author |        |          |
| --------- | ------ | -------- |
| 字段名       | 类型     | 说明       |
| bot       | bool   | 该成员是否机器人 |
| id        | string | 该成员ID    |
| username  | string | 该成员昵称    |
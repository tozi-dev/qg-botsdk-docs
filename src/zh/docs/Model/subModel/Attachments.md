# Attachments
## 结构
| 字段名            | 类型     | 说明                         |
| -------------- | ------ | -------------------------- |
| content_type   | sring  | 附件类型，如：image/jpeg          |
| filename       | string | 附件名，如：504BA0……6A509E68.jpg |
| height         | int    | 高度                         |
| width          | int    | 宽度                         |
| size           | int    | 附件大小                       |
| id             | string | 附件ID                       |
| url            | string | 附件url                      |
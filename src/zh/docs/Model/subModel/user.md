# 用户
## 结构
| 字段名       | 类型     | 说明          |
| --------- | ------ | ----------- |
| avatar    | sring  | 该成员头像图片url  |
| bot       | bool   | 该成员是否机器人    |
| id        | string | 该成员的ID      |
| username  | list   | 该成员在频道全局的昵称 |
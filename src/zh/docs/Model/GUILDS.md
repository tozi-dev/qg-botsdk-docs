# GUILDS

## 结构
| 字段名          | 类型     | 说明                                         |
| ------------ | ------ | ------------------------------------------ |
| description  | sring  | 频道介绍                                       |
| icon         | sring  | 频道头像图片url                                  |
| id           | sring  | 频道ID（guild id）                             |
| joined_at    | sring  | 机器人加入频道时间，具体格式例如：$2021-10-21T11:20:18$+08:00 |
| max_members  | int    | 频道最大成员数                                    |
| member_count | int    | 频道当前成员数                                    |
| name         | string | 频道名                                        |
| op_user_id   | string | 操作人ID                                      |
| owner_id     | string | 频道拥有者ID                                    |
| t            | string | 事件类型字段，如`GUILD_CREATE`                       |
| event_id     | sring  | 事件ID                                       |
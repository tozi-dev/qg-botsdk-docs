---
title: 文档
icon: book
---
# 文档
QG-botsdk主要包括如下部分：
- BOT类 *qg-botsdk核心*
- API *用于与官方接口交互*
- [Model](./Model/) *用于类型提示及一定程度上的代码可行性校验*

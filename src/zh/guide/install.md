---
icon: add
---
# 安装

QG-botsdk支持通过pypi或源代码安装：

::: code-tabs#shell

@tab:active pip

```bash
pip install qg-botsdk
```

@tab pipx

```bash
pipx install qg-botsdk
```

@tab git

```bash
git clone git@github.com:GLGDLY/qg_botsdk.git
cd ./qg_botsdk
python3 ./setup.py install
```

:::
import { defineUserConfig } from "vuepress";
import theme from "./theme.js";

export default defineUserConfig({
  base: "/",

  locales: {
    "/": {
      lang: "en-US",
      title: "QG-botsdk",
      description: "A docs demo for vuepress-theme-hope",
    },
    "/zh/": {
      lang: "zh-CN",
      title: "QG-botsdk",
      description: "快速、高效的QQ频道机器人实现",
    },
  },

  theme,

  // Enable it with pwa
  // shouldPrefetch: false,
});

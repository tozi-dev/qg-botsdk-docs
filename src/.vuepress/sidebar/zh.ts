import { sidebar } from "vuepress-theme-hope";

export const zhSidebar = sidebar({
  "/zh/guide/": [
    {
      text: "教程",
      icon: "book",
      prefix: "",
      children: "structure",
    }
  ],
  "/zh/docs/": [
    {
      text: "文档",
      icon: "book",
      children: "structure",
    }
  ]
});
